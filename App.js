import React, { Component } from "react";
import { View, Button, StyleSheet, Text } from "react-native";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center"
  },
  containerOptions:{
    padding: 15,
    margin: 5,
  },
  buttonContainer: {
    margin: 5,
    flexDirection: "row",
    justifyContent: "space-between"
  }
});

export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true
    };
    this._command = this._command.bind(this);
  }

  _command({ client, ipwall, door, command }) {
    fetch(`http://192.168.0.247:8001/Client/${client}/Commands/user/${client}/ipwall/${ipwall}/door/${door}/${command}`)
      .then(response => response.json())
      .catch(error => {
        console.error(error);
      });
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.containerOptions}>
          <Text>Acesso Principal</Text>
          <View style={styles.buttonContainer}>
            <Button onPress={() => this._command({client : 8,ipwall: 1,door: 1, command: 'open'})} title="Open" />
            <Button onPress={() => this._command({client : 8,ipwall: 1,door: 1, command: 'keepTheDoorOpened'})} title="Keep Open" />
            <Button onPress={() => this._command({client : 8,ipwall: 1,door: 1, command: 'close'})} title="Close" />
          </View>
        </View>
        <View style={styles.containerOptions}>
          <Text>Acesso Segware</Text>
          <View style={styles.buttonContainer}>
          <Button onPress={() => this._command({client : 8,ipwall: 10,door: 1, command: 'open'})} title="Open" />
          <Button onPress={() => this._command({client : 8,ipwall: 10,door: 1, command: 'keepTheDoorOpened'})} title="Keep Open" />
          <Button onPress={() => this._command({client : 8,ipwall: 10,door: 1, command: 'close'})} title="Close" />
          </View>
        </View>
        <View style={styles.containerOptions}>
          <Text>Acesso Kiper</Text>
          <View style={styles.buttonContainer}>
          <Button onPress={() => this._command({client : 8,ipwall: 9,door: 1, command: 'open'})} title="Open" />
          <Button onPress={() => this._command({client : 8,ipwall: 9,door: 1, command: 'keepTheDoorOpened'})} title="Keep Open" />
          <Button onPress={() => this._command({client : 8,ipwall: 9,door: 1, command: 'close'})} title="Close" />
          </View>
        </View>
      </View>


    );
  }
}
